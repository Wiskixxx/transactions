﻿using Microsoft.EntityFrameworkCore;
using Transactions.DataAccess.Entities;

namespace Transactions.DataAccess
{
    public class TransactionContext : DbContext
    {
        public TransactionContext(DbContextOptions options) : base(options)
        {
            Database.Migrate();
        }

        public DbSet<PaymentDetail> PaymentDetails { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
    }
}
