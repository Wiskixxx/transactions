﻿namespace Transactions.DataAccess.Common
{
    public enum TransactionStatus
    {
        Approved,
        Failed,
        Finished
    }
}
