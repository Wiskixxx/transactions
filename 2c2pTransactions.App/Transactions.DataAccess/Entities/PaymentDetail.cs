﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Transactions.DataAccess.Entities
{
    [Table("Transaction")]
    public class PaymentDetail
    {
        [Key]
        [Required]
        [MaxLength(50)]
        public string TransactionId { get; set; }
        [Required]
        public decimal Amount { get; set; }
        [Required]
        [StringLength(3, MinimumLength = 3)]
        public string Currency { get; set; }
        public virtual Transaction Transaction { get; set; }

    }
}
