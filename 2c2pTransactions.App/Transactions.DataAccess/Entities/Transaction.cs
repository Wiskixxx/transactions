﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Transactions.DataAccess.Common;

namespace Transactions.DataAccess.Entities
{
    [Table("Transaction")]
    public class Transaction
    {
        [Required]
        [MaxLength(50)]
        public string TransactionId { get; set; }
        public TransactionStatus Status { get; set; }
        public DateTime Date { get; set; }
        public virtual PaymentDetail PaymentDetail { get; set; }
    }
}
