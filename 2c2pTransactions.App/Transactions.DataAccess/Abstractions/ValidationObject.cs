﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Transactions.DataAccess.Abstractions
{
    public abstract class ValidationObject<TObjectToValidate>
    {
        public List<ValidationResult> Validate()
        {
            var validationContext = new ValidationContext(this, null, null);
            var validationResults = new List<ValidationResult>();

            IsValid = Validator.TryValidateObject(this, validationContext, validationResults, true);
            if (!IsValid)
            {
                ValidationMessage.AddRange(validationResults.Select(v => v.ErrorMessage));
            }

            return validationResults;
        }
        public bool IsValid { get; private set; } = true;
        public List<string> ValidationMessage { get; protected set; } = new List<string>();
    }
}
