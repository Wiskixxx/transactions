﻿using System;
using System.ComponentModel.DataAnnotations;
using Transactions.DataAccess.Abstractions;
using Transactions.DataAccess.Attributes;
using Transactions.DataAccess.Common;

namespace Transactions.DataAccess.DTO
{
    public class TransactionDTO : ValidationObject<TransactionDTO>
    {
        public TransactionDTO(string transationId, decimal amount, string currency, string status, DateTime date)
        {
            TransationId = transationId;
            Amount = amount;
            Currency = currency;
            Status = MapTransactionStatus(status);
            Date = date;
            Validate();
        }
        [Required]
        [MaxLength(50)]
        public string TransationId { get; }
        [Required]
        public decimal Amount { get; }
        [Currency]
        public string Currency { get; }
        public TransactionStatus? Status { get; }
        public DateTime Date { get; }
        private TransactionStatus? MapTransactionStatus(string status)
        {
            switch (status)
            {
                case "Approved":
                    return TransactionStatus.Approved;
                case "Rejected":
                case "Failed":
                    return TransactionStatus.Failed;
                case "Done":
                case "Finished":
                    return TransactionStatus.Finished;
                default:
                    ValidationMessage.Add("Invalid Status Field");
                    return null;
            }
        }
    }
}
