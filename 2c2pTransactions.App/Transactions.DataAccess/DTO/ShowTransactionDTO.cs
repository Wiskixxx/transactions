﻿using System.ComponentModel;
using Transactions.DataAccess.Common;

namespace Transactions.DataAccess.DTO
{
    public class ShowTransactionDTO
    {
        public ShowTransactionDTO(string transationId, decimal amount, string currency, TransactionStatus transactionStatus)
        {
            TransationId = transationId;
            Amount = amount;
            Currency = currency;
            Status = MapTransactionStatus(transactionStatus);
        }

        public string TransationId { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string Status { get; set; }

        private string MapTransactionStatus(TransactionStatus status)
        {
            switch (status)
            {
                case TransactionStatus.Approved:
                    return "A";
                case TransactionStatus.Failed:
                    return "R";
                case TransactionStatus.Finished:
                    return "D";
                default:
                    throw new InvalidEnumArgumentException();
            }
        }
    }
}
