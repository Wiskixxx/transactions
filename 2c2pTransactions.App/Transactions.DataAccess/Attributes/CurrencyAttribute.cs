﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;

namespace Transactions.DataAccess.Attributes
{
    public class CurrencyAttribute : ValidationAttribute
    {
        private const string _validationMessage = "Invalid Currency";
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var currency = value as string;

            if (string.IsNullOrWhiteSpace(currency))
            {
                return new ValidationResult(_validationMessage);
            }

            var cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures).ToList();
            var IsoCurrencyList = cultures.Select(c => new RegionInfo(c.Name).ISOCurrencySymbol);

            if (IsoCurrencyList.Contains(value))
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(_validationMessage);
            }
        }
    }
}
