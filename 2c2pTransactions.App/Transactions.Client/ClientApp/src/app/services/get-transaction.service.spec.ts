import { TestBed } from '@angular/core/testing';

import { GetTransactionService } from './get-transaction.service';

describe('GetTransactionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetTransactionService = TestBed.get(GetTransactionService);
    expect(service).toBeTruthy();
  });
});
