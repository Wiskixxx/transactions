import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from  '@angular/common/http';  
import { map } from  'rxjs/operators';

@Injectable({  
  providedIn: 'root'  
})  
export class UploadFileService {
  constructor(private httpClient: HttpClient) { }
  
  public upload(formData) {

    return this.httpClient.post<any>(`api/file`, formData, {  
        reportProgress: true,  
        observe: 'events'  
      });  
  }
}
