import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from  '@angular/common/http';  
import { map } from  'rxjs/operators';
import { Transaction } from '../Model/Transaction';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GetTransactionService {

  constructor(private httpClient: HttpClient) { }
  
  serialize(obj) : string {
    var str = [];
    for (var p in obj)
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
      }
    return str.join("&");
  }

  public get(target, filter) : Observable<Transaction[]> {
    const urlFilter = this.serialize(filter);
    return this.httpClient.get<Transaction[]>(`api/transaction/${target}?${urlFilter}`);  
  }
}
