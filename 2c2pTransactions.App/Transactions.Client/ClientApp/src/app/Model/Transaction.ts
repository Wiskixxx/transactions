export class Transaction {
    public transationId: string
    public amount: number
    public currency: string
    public status: string
}