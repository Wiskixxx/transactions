import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Transaction } from 'src/app/Model/Transaction';


@Component({
  selector: 'app-show-transaction',
  templateUrl: './show-transaction.component.html',
  styleUrls: ['./show-transaction.component.css']
})
export class ShowTransactionComponent implements OnInit {

  @Input() public transactions$: Observable<Transaction[]>

  constructor() { }

  ngOnInit() {
  }

}
