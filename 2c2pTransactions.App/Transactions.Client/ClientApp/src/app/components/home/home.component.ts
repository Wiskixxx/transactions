import { Component } from '@angular/core';
import { GetTransactionService } from  '../../services/get-transaction.service';
import { Observable } from 'rxjs';
import { Transaction } from 'src/app/Model/Transaction';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
  public transactions$ : Observable<Transaction[]>
  constructor(private transactionService: GetTransactionService){
    
  }

  onFilterChange(obj: any){
    this.transactions$ = this.transactionService.get(obj.target, obj.filter);
  }
}
