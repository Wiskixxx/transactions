import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { GetTransactionService } from 'src/app/services/get-transaction.service';

@Component({
  selector: 'app-transaction-filter',
  templateUrl: './transaction-filter.component.html',
  styleUrls: ['./transaction-filter.component.css']
})
export class TransactionFilterComponent implements OnInit {
  @Output() public filterChange: EventEmitter<any> = new EventEmitter();
  public filterOpt: string[] = ['status', 'date', 'currency'];
  public statuses: string[] = ['Approved', 'Failed', 'Finished']
  public selectedStatus: string = 'Approved'
  public selectedFilter = this.filterOpt[2]

  constructor(private getTransactionService: GetTransactionService) { }

  ngOnInit() {
  }

  changeSelectedFilter(newFilterOpt: string) { 
    this.selectedFilter = newFilterOpt;
  }

  selectStatus(status){
    this.selectedStatus = status;
  }

  onSubmit(filter){
    this.filterChange.emit({ target: this.selectedFilter, filter })
  }

}
