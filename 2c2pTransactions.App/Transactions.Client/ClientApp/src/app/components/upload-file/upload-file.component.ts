import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';  
import { catchError, map } from 'rxjs/operators';  
import { UploadFileService } from  '../../services/upload-file.service';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.css']
})
export class UploadFileComponent implements OnInit {
  @ViewChild("fileUpload", {static: false}) fileUpload: ElementRef;
  file: any = {};  
  errors = [];
  isLoading = false;
  isSent = false;
  constructor(private uploadService: UploadFileService) { }

  ngOnInit() {
  }

  uploadFile(file) {  
    const formData = new FormData();  
    this.isLoading = true;
    formData.append('file', file);  
    this.uploadService.upload(formData).pipe(
      map(event => {  
        if (event.type === HttpEventType.Response) {  
          this.isSent = true;
          this.isLoading = false;
        }  
      }),
      catchError((error: HttpErrorResponse) => { 
        this.isLoading = false; 
        if (Array.isArray(error.error[0])){
          this.errors = error.error[0];
        }
        else{
          this.errors = error.error;
        }
        return of(`${file.name} upload failed.`);  
      })).subscribe();  
  }

  onSubmit() {  
    this.fileUpload.nativeElement.value = '';  
    this.uploadFile(this.file);
  }

  handleFileInput(files: FileList){
    this.file = Array.from(files)[0];
  }

  removeFile() {
    this.file = null;
    this.errors = null;
  }

  onClick() {  
    this.removeFile();
    const fileUpload = this.fileUpload.nativeElement;
    fileUpload.click();  
  }

}
