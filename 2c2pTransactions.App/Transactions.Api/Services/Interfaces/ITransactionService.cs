﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Transactions.DataAccess.DTO;
using Transactions.DataAccess.Entities;

namespace Transactions.Api.Services.Interfaces
{
    public interface ITransactionService
    {
        Task SaveTransactions(List<TransactionDTO> transactions);
        IEnumerable<ShowTransactionDTO> GetTransactions(Func<PaymentDetail, bool> filter);
    }
}
