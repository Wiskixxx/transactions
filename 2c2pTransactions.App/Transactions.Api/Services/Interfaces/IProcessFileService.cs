﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Transactions.DataAccess.DTO;

namespace Transactions.Api.Services.Interfaces
{
    public interface IProcessFileService
    {
        List<TransactionDTO> ProcessFile([NotNull] IFormFile formFile);
        List<string> Errors { get; }
    }
}
