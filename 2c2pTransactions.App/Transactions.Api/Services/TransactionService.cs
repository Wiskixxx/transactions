﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transactions.Api.Properties;
using Transactions.Api.Services.Interfaces;
using Transactions.DataAccess;
using Transactions.DataAccess.DTO;
using Transactions.DataAccess.Entities;

namespace Transactions.Api.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly TransactionContext _context;
        private readonly ILogger<TransactionService> _logger;

        public TransactionService(TransactionContext context, ILogger<TransactionService> logger)
        {
            _context = context;
            _logger = logger;
        }

        public IEnumerable<ShowTransactionDTO> GetTransactions(Func<PaymentDetail, bool> filter)
        {
            var filtered = _context.PaymentDetails.Include(t => t.Transaction).Where(filter);
            return filtered.Select(t => new ShowTransactionDTO(t.TransactionId, t.Amount, t.Currency, t.Transaction.Status));
        }

        public async Task SaveTransactions(List<TransactionDTO> transactions)
        {
            try
            {
                var tr = transactions.Select(t => 
                    new Transaction { 
                            TransactionId = t.TransationId,
                            Date = t.Date,
                            Status = t.Status.Value,
                            PaymentDetail = new PaymentDetail { TransactionId = t.TransationId, Amount = t.Amount, Currency = t.Currency }
                        }
                    );
                await _context.Transactions.AddRangeAsync(tr);
                await _context.SaveChangesAsync();
            }
            catch(Exception e)
            {
                _logger.LogError(e, Resources.DublicateTransactionsException);
                throw;
            }
        }
    }
}
