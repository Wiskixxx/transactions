﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using Transactions.Api.Managers.Interfaces;
using Transactions.Api.Properties;
using Transactions.Api.Services.Interfaces;
using Transactions.DataAccess.DTO;

namespace Transactions.Api.Services
{
    public class ProcessFileService : IProcessFileService
    {
        private readonly IFileParserManager _fileParserManager;
        private readonly ILogger<ProcessFileService> _logger;
        // TODO: tried to go with Proxy for ILogger but there are issues, needed deeper investigation
        private readonly List<string> _processFileErrorsCollector = new List<string>();

        public ProcessFileService(IFileParserManager fileParserManager, ILogger<ProcessFileService> logger)
        {
            _fileParserManager = fileParserManager;
            _logger = logger;
        }

        public List<string> Errors => _processFileErrorsCollector;

        public List<TransactionDTO> ProcessFile([NotNull] IFormFile formFile)
        {
            var transactions = new List<TransactionDTO>();

            var extention = Path.GetExtension(formFile.FileName);
            var parser = _fileParserManager.GetFileParser(extention);

            if (parser is null)
            {
                LogFileProcess(Resources.InvalidFileExtention);

                return transactions;
            }

            using (var s = formFile.OpenReadStream())
            {
                try
                {
                    transactions = parser.Parse(s);
                }
                catch (Exception e)
                {
                    LogFileProcess(e.Message);
                }
            }

            return transactions;
        }

        private void LogFileProcess(string message)
        {
            _logger.LogInformation(message);
            _processFileErrorsCollector.Add(message);
        }
    }
}
