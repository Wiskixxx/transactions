﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Transactions.Api.Services.Interfaces;
using Transactions.DataAccess.DTO;

namespace Transactions.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly ITransactionService _transactionService;

        public TransactionController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }
        [Route("currency")]
        [HttpGet]
        public IEnumerable<ShowTransactionDTO> GetByCurrency([FromQuery]string cur)
        {
            return _transactionService.GetTransactions(t => t.Currency == cur);
        }
        [Route("date")]
        [HttpGet]
        public IEnumerable<ShowTransactionDTO> GetByDateRange([FromQuery]DateTime start, [FromQuery]DateTime end)
        {
            return _transactionService.GetTransactions(t => t.Transaction.Date >= start && t.Transaction.Date <= end);
        }
        [Route("status")]
        [HttpGet]
        public IEnumerable<ShowTransactionDTO> GetByStatus([FromQuery]string status)
        {
            return _transactionService.GetTransactions(t => t.Transaction.Status.ToString() == status);
        }
    }
}