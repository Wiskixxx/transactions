﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;
using Transactions.Api.Properties;
using Transactions.Api.Services.Interfaces;

namespace Transactions.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly IProcessFileService _processFileService;
        private readonly ITransactionService _transactionService;
        private readonly ILogger _logger;

        public FileController(IProcessFileService validateFileService, ITransactionService transactionService, ILogger<FileController> logger)
        {
            _processFileService = validateFileService;
            _transactionService = transactionService;
            _logger = logger;
        }
        // POST api/<controller>
        [HttpPost]
        public async Task<IActionResult> Post()
        {
            
            if (!Request.Form.Files?.Any() ?? false)
                return BadRequest();

            var file = Request.Form.Files.First();

            var transactions = _processFileService.ProcessFile(file);

            if (_processFileService.Errors.Any())
                return BadRequest(_processFileService.Errors);

            if (transactions?.Where(v => !v.IsValid).Any() ?? false)
                return BadRequest(transactions.Select(t => t.ValidationMessage));
            try
            {
                await _transactionService.SaveTransactions(transactions);
                
                return Ok(transactions);
            }
            catch(Exception e)
            {
                return BadRequest(new[] { Resources.DublicateTransactionsException });
            }
        }
    }
}
