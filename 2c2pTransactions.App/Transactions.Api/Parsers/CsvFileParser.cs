﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Transactions.Api.Parsers.Interfaces;
using Transactions.Api.Properties;
using Transactions.DataAccess.DTO;

namespace Transactions.Api.Parsers
{
    public class CsvFileParser : IFileParser
    {
        public List<TransactionDTO> Parse(Stream fileStream)
        {
            var transactions = new List<TransactionDTO>();
            using (var reader = new StreamReader(fileStream))
            {
                try
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split("\",")?.Select(v => v.Trim().Replace("\"", string.Empty)).ToArray();

                        if (values?.Any() ?? false)
                        {
                            values[1] = values[1].Replace(",", string.Empty);
                            decimal.TryParse(values[1]
                                , System.Globalization.NumberStyles.AllowDecimalPoint
                                , CultureInfo.InvariantCulture
                                , out decimal amount);

                            var format = "dd/MM/yyyy hh:mm:ss";
                            DateTime.TryParseExact(values[3], format, CultureInfo.InvariantCulture, DateTimeStyles.None, out var dateTime);

                            var t = new TransactionDTO(values[0], amount, values[2], values[4], dateTime);

                            transactions.Add(t);
                        }
                    }
                }
                catch(Exception e)
                {
                    throw new Exception(Resources.CSVFileFormatException);
                }

                return transactions;
            }
        }
    }
}
