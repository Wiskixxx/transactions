﻿using System.Collections.Generic;
using System.IO;
using Transactions.DataAccess.DTO;

namespace Transactions.Api.Parsers.Interfaces
{
    public interface IFileParser
    {
        List<TransactionDTO> Parse(Stream fileStream);
    }
}
