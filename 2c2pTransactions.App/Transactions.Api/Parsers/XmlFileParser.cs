﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml.Linq;
using Transactions.Api.Parsers.Interfaces;
using Transactions.Api.Properties;
using Transactions.DataAccess.DTO;

namespace Transactions.Api.Parsers
{
    public class XmlFileParser : IFileParser
    {
        public List<TransactionDTO> Parse(Stream fileStream)
        {
            var transactions = new List<TransactionDTO>();
            // TODO: change to deserialization
            XDocument xdoc = XDocument.Load(fileStream);
            try
            {
                foreach (XElement transaction in xdoc.Element("Transactions").Elements("Transaction"))
                {
                    string transactionId = transaction.Attribute("id").Value;
                    string transactionDate = transaction.Element("TransactionDate").Value;
                    string transactionStatus = transaction.Element("Status").Value;
                    XElement paymentDetails = transaction.Element("PaymentDetails");
                    string transactionAmount = paymentDetails.Element("Amount").Value;
                    string transactionCurrency = paymentDetails.Element("CurrencyCode").Value;

                    DateTime.TryParse(transactionDate, out var dateTime);

                    transactionAmount = transactionAmount.Replace(",", string.Empty);
                    decimal.TryParse(transactionAmount
                        , System.Globalization.NumberStyles.AllowDecimalPoint
                        , CultureInfo.InvariantCulture
                        , out decimal amount);

                    var trans = new TransactionDTO(transactionId, amount, transactionCurrency, transactionStatus, dateTime);

                    transactions.Add(trans);
                }
            }
            catch(Exception e)
            {
                throw new Exception(Resources.XMLFileFormatException);
            }

            return transactions;
        }
    }
}
