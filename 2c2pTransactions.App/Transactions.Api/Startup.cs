using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Transactions.Api.Managers;
using Transactions.Api.Managers.Interfaces;
using Transactions.Api.Parsers;
using Transactions.Api.Services;
using Transactions.Api.Services.Interfaces;
using Transactions.DataAccess;

namespace Transactions.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            string connection = Configuration.GetConnectionString("TransactionDb");

            services.AddDbContextPool<TransactionContext>(options =>
                options.UseSqlServer(connection));

            services.AddSingleton(RegisterFileManager());
            services.AddScoped<IProcessFileService, ProcessFileService>();
            services.AddScoped<ITransactionService, TransactionService>();
        }

        private IFileParserManager RegisterFileManager()
        {
            var fileManager = new FileParserManager();
            fileManager.RegisterParser("csv", new CsvFileParser());
            fileManager.RegisterParser("xml", new XmlFileParser());

            return fileManager;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            
        }
    }
}
