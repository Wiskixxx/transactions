﻿using Transactions.Api.Parsers.Interfaces;

namespace Transactions.Api.Managers.Interfaces
{
    public interface IFileParserManager
    {
        void RegisterParser(string token, IFileParser fileParser);
        IFileParser GetFileParser(string token);
    }
}
