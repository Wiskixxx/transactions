﻿using System.Collections.Generic;
using Transactions.Api.Managers.Interfaces;
using Transactions.Api.Parsers.Interfaces;

namespace Transactions.Api.Managers
{
    public class FileParserManager : IFileParserManager
    {
        private readonly Dictionary<string, IFileParser> _parsers = new Dictionary<string, IFileParser>();
        public IFileParser GetFileParser(string token)
        {
            token = token.Replace(".", string.Empty);
            return _parsers[token];
        }

        public void RegisterParser(string token, IFileParser fileParser)
        {
            if (!_parsers.ContainsKey(token))
                _parsers.Add(token, fileParser);
        }
    }
}
